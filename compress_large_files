#!/bin/bash
#
# Recursively compress any files greater than a supplied MINSIZE (-s), starting
# from the supplied DIRECTORY (-d).
#

error() {
    echo "Error: $1"
    exit 1
}

usage() {
    echo "Usage: $0 <options>

Where <options> include:
   -d DIRECTORY - Specify directory to recurse
   -s MINSIZE   - Only compress files larger than MINSIZE
   -a AGE       - Only compress files modified more than AGE minutes ago (default 60)
   -n           - Don't make changes, just display
"
    exit 1
}

# Initialize variables
COMPRESS_SUFFIX=".gz"
COMPRESS_CMD="gzip -v --suffix ${COMPRESS_SUFFIX}"

# Process cmdline arguments
DRYRUN=0
MINSIZE=""
MINAGE=60
COMPRESS_DIR=""
while getopts "ns:a:d:h" options; do
  case $options in
    n ) DRYRUN=1 ;;
    s ) MINSIZE=$OPTARG ;;
    a ) MINAGE=$OPTARG ;;
    d ) COMPRESS_DIR=$OPTARG ;;
    \?|h ) usage ;;
    * ) usage ;;
  esac
done

# Validate input
test -z "${COMPRESS_DIR}" && error "Missing -d argument"
test -z "${MINSIZE}" && error "Missing -s argument"
test -z "${MINAGE}" && error "Missing -a argument"

# Find files to compress using the supplied arguments
#  - larger than MINSIZE Mib
#  - older than MINAGE minutes
#  - not already compressed
find "${COMPRESS_DIR}" -type f -not -name "*${COMPRESS_SUFFIX}" -mmin +"${MINAGE}" -size +"${MINSIZE}"M |
while read line
do
    if [ ${DRYRUN} -eq 1 ]; then
        echo "\$ ${COMPRESS_CMD} ${line}"
        echo "\$ ln -s $(basename $line).gz ${line}"
    else
        ${COMPRESS_CMD} ${line}
        # Create symlink to original file
        ln -s $(basename $line).gz ${line}
    fi
done

